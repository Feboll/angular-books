angular.
  module('core.rest').
  factory('Book', ['$resource',
    function($resource) {
      return $resource('books/:id.json', {}, {
        query: {
          method: 'GET',
          params: {id: 'books'},
          isArray: true
        }
      });
    }
  ]);
