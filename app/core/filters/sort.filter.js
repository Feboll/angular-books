angular.
module('core').
filter('sort', function() {
    return function(item, reverse, name) {
      var currentClass = '';
      if (item.sortField === name && !reverse) {
        currentClass = 'sorting__item-current sorting__item-current-up'
      } else if (item.sortField === name && reverse) {
        currentClass = 'sorting__item-current';
      }
        return currentClass;
    };
});
