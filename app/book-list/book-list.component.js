// Define the `PhoneListController` controller on the `phonecatApp` module
angular.module('bookList').component('bookList', {
    templateUrl: 'book-list/book-list.template.html',
    controller: ['Book', function BookListController(Book) {
        var self = this;
        self.sortField = undefined;
        self.reverse = false;

        self.books = Book.query();

        self.sort = function(fieldName) {
          self.sortField = fieldName;
          self.reverse = !self.reverse;
        }
    }]
});
