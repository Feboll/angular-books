angular.
  module('booksApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      // $locationProvider.html5Mode({
      //       enabled: true,
      //       requireBase: false
      //   });
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/', {
          template: '<book-list></book-list>'
        }).
        when('/books/:id', {
          template: '<book-detail></book-detail>'
        }).
        otherwise('/');
    }
  ]);
