// Define the `booksApp` module
angular.module('booksApp', [
    'ngAnimate',
    'ngRoute',
    'core',
    'bookList',
    'bookDetail'
]);
