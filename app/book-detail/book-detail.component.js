angular.
  module('bookDetail').
  component('bookDetail', {
    templateUrl: 'book-detail/book-detail.template.html',
    controller: ['Book', '$routeParams',
      function PhoneDetailController(Book, $routeParams) {
        var self = this;

        self.book = Book.get({id: $routeParams.id}, function(book) {
          self.setImage(book.imageUrl)
        });

        self.setImage = function setImage(imageUrl) {
            self.mainImage = imageUrl;
        }
      }
    ]
  });
